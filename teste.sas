
%macro mapeia_variaveis(libname,tabela);
  /* ============================================================= */
  /* Detecta tipo de variáveis e monta comandos que serão chamados */
  /* ============================================================= */
  PROC SQL NOPRINT;
    SELECT
        '"' || STRIP(NAME) || '"n'
        
      , CASE WHEN TYPE='char' THEN "'" || STRIP(NAME) || "'n" END
      , CASE WHEN TYPE='num'  THEN "'" || STRIP(NAME) || "'n" END
      
      , 'COUNT (DISTINCT "' || STRIP(NAME) || '"n) AS DIST' || STRIP(PUT(VARNUM, 15.))

      , '"' || STRIP(NAME) || '"n = "VARNUM' || STRIP(PUT(VARNUM, 15.)) || '"n'
      
      , 'TABLE "VARNUM' || STRIP(PUT(VARNUM, 15.)) || '"n / MISSING OUT=_TEMP_F_VAR' || STRIP(PUT(VARNUM, 15.)) || "(RENAME=(VARNUM" || STRIP(PUT(VARNUM, 15.)) || "=DETALHE COUNT=RESULT) DROP=PERCENT);" 

      , MAX(VARNUM)
    INTO
        :var_allvars SEPARATED BY ','
      
      , :var_char SEPARATED BY ' '
      , :var_num  SEPARATED BY ' '
      
      , :var_distinct       SEPARATED BY ','
            
      , :var_rename         SEPARATED BY ' '
      
      , :var_procfreq_table_output SEPARATED BY ' '
      
      , :var_qnte
    FROM
      DICTIONARY.COLUMNS
    WHERE
          upcase("LIBNAME"n) = upcase("&libname.")
      AND upcase("MEMNAME"n) = upcase("&tabela.")
    ORDER BY
      VARNUM
    ;  
  
  /* =========================================== */
  /* Obtém informação de quantidade de distintos */
  /* =========================================== */
  PROC SQL NOPRINT;
    CREATE TABLE _TEMP_DISTINCT_T AS
    SELECT &var_distinct. FROM &libname..&tabela.;
  PROC TRANSPOSE DATA=WORK._TEMP_DISTINCT_T OUT=WORK._TEMP_DISTINCT(RENAME=(Column1=RESULT)) name=VAR PREFIX=Column;
    var _ALL_;
  RUN;
  DATA _TEMP_DISTINCT; VARNUM = _N_; LENGTH METRIC $ 80; SET _TEMP_DISTINCT; METRIC = "NDistinct"; DROP VAR; RUN;
  %SYMDEL var_distinct var_distinct_vars / NOWARN ; 
  PROC DATASETS LIB = WORK NOLIST NOWARN; DELETE _TEMP_DISTINCT_T; RUN;

  /* =================================================== */
  /* Obtém estatísticas básicas para variáveis numéricas */
  /* =================================================== */
  PROC MEANS DATA=&libname..&tabela.(RENAME=(&var_rename.)) NOPRINT;
    VAR _NUMERIC_;
    OUTPUT OUT=_TEMP_NUMMETRIC_T(DROP=_FREQ_ _TYPE_) MAX= MIN= MEAN= STD= CV= P1= P5= P10= P25= P50= P75= P90= P95= P99= N= NMISS= / AUTONAME;
  RUN;
  PROC TRANSPOSE DATA=WORK._TEMP_NUMMETRIC_T OUT=WORK._TEMP_NUMMETRIC(RENAME=(Column1=RESULT)) name=VAR PREFIX=Column;
    var _ALL_;
  RUN;
  DATA _TEMP_NUMMETRIC;
    LENGTH METRIC $ 80; 
    
    SET _TEMP_NUMMETRIC(DROP=_LABEL_);
    
    VARNUM = STRIP(COMPRESS(TRANWRD(VAR, "VARNUM", "")));
    LOC = INDEX(VARNUM,"_");
    METRIC = STRIP(SUBSTR(VARNUM, LOC+1, LENGTH(VARNUM)-LOC));
    VARNUM_N = INPUT(SUBSTR(VARNUM, 1, LOC-1), BEST9.);
    
    DROP VARNUM VAR LOC;
    RENAME VARNUM_N = VARNUM;
  RUN;
  PROC DATASETS LIB = WORK NOLIST NOWARN; DELETE _TEMP_NUMMETRIC_T; RUN;
  
  /* ============================================================= */
  /* Concatena as bases de dados de distintos e métricas numéricas */
  /* ============================================================= */
  DATA _TEMP_COMP; SET _TEMP_DISTINCT _TEMP_NUMMETRIC; RUN;  
  PROC SORT DATA = _TEMP_COMP;
    BY VARNUM METRIC;
  RUN;
  PROC DATASETS LIB = WORK NOLIST NOWARN; DELETE _TEMP_NUMMETRIC _TEMP_DISTINCT; RUN;
  
  /* ================================================================== */
  /* Calcula frequências com PROC FREQ (gerará uma tabela por variável) */
  /* ================================================================== */
  PROC FREQ DATA=&libname..&tabela.(RENAME=(&var_rename.)) NOPRINT order=freq;
    &var_procfreq_table_output.;
  RUN;
  
  /* -------------------------------------------------------------------------- */
  /* Cria um dataset com metadados, para operar adequadamente a macro posterior */
  DATA _META_FREQ_T;
    I = 1;
    DO WHILE (I <= &var_qnte.);
      DATASET = "_TEMP_F_VAR" || STRIP(PUT(I, 15.));
      VARNUM = "VARNUM"|| STRIP(PUT(I, 15.));
      VARNUM_N = I;
      OUTPUT;
      I = I + 1;
    END;
  RUN;
  PROC SQL;
    CREATE TABLE _META_FREQ AS
    SELECT
        T1.*
      , CASE WHEN T2.TYPE = "num"  AND MISSING(FORMAT)      THEN "BEST32."
             WHEN T2.TYPE = "char" AND NOT MISSING(FORMAT)  THEN T2.FORMAT
             ELSE "CHAR" || STRIP(PUT(T2.LENGTH, BEST32.)) || "."
        END AS FORMAT
    FROM
      _META_FREQ_T AS T1
      INNER JOIN
      DICTIONARY.COLUMNS AS T2 ON (     T1.VARNUM_N = T2.VARNUM 
                                    AND UPCASE(T2."LIBNAME"n) = UPCASE("&libname.")
                                    AND UPCASE(T2."MEMNAME"n) = UPCASE("&tabela.")
                                  );
  RUN;
  
  /* Macro para compilar os datasets de frequencia */
  %macro ajusta_freq(DATASET, VARNUM, FORMAT);
    DATA &dataset.;
      VARNUM = &varnum.;
      METRIC = "z_Mode";
      
      SET &dataset.;
      
      DETALHE_T = STRIP(PUT(DETALHE, &format.));
      
      DROP DETALHE;
      RENAME DETALHE_T = DETALHE;
    RUN;    
  %mend ajusta_freq;
  DATA _NULL_;
    SET _META_FREQ; CALL EXECUTE('%ajusta_freq(' || DATASET || ',' || VARNUM_N || ',' || FORMAT || ');');
  RUN;

  /* Busca por SQL qual é a maior comprimento */
  PROC SQL NOPRINT; 
    SELECT MAX(LENGTH) INTO :detalhe_max_length FROM DICTIONARY.COLUMNS
                  WHERE UPCASE("LIBNAME"n) = UPCASE("WORK")
                    AND UPCASE("MEMNAME"n) LIKE "_TEMP_F_VAR_"
                    AND UPCASE("NAME"n) = UPCASE("DETALHE");
    %put max_length = &detalhe_max_length.;
    
    SELECT DATASET INTO :var_datasets_moda SEPARATED BY ' ' FROM _META_FREQ;
    
  /* Cria tabela com número máximo de observações */
  PROC SQL NOPRINT;
    SELECT NOBS INTO :max_obs FROM DICTIONARY.TABLES
                                  WHERE UPCASE("LIBNAME"n) = UPCASE("&libname.")
                                    AND UPCASE("MEMNAME"n) = UPCASE("&tabela.");
  DATA _TEMP_NOBS;
    SET _META_FREQ;
    
    RESULT = &max_obs.;
    METRIC = "NObs";
    
    KEEP METRIC VARNUM_N RESULT;
    
    RENAME VARNUM_N = VARNUM;
  RUN;
  
  
  /* Cria tabela final */
  DATA _TEMP_RESULT;
    LENGTH DETALHE $ &detalhe_max_length.; 
    SET _TEMP_COMP _TEMP_NOBS &var_datasets_moda.;
  RUN;
  
  /* Ordena a tabela */
  PROC SORT DATA=_TEMP_RESULT OUT=RESULT_VARMAP;
    BY VARNUM METRIC;
  RUN;
  
  /* Limpa a Work */
  PROC DATASETS LIB=WORK NOLIST;
    DELETE _TEMP_COMP _TEMP_RESULT _TEMP_NOBS _META_FREQ _META_FREQ_T &var_datasets_moda.;
  RUN;
  
%mend mapeia_variaveis;




%mapeia_variaveis(sashelp,baseball);
